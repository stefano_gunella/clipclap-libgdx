package com.hell.gun.glx;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.badlogic.gdx.backends.android.AndroidFragmentApplication;

public class GlxGameFragment extends AndroidFragmentApplication{
    public GlxGameFragment() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	 return initializeForView(new GlxGameView());
    }
}