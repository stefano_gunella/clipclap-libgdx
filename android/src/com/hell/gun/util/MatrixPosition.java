package com.hell.gun.util;

import javax.microedition.khronos.opengles.GL10;

import android.annotation.SuppressLint;
import android.util.Log;

import com.hell.gun.GameParameters;
import com.hell.gun.Square;

public class MatrixPosition {
	private static String TAG = "MatrixPosition";
	public static Square[][] mSquares = null;
	public static Square[][] subSquares = null;
	public static GL10 gl = null;
	
	//	public void testMatrix(){
	//		Matrix a = new Matrix(array); 
	//		Matrix b = new Matrix(new double[]{1., 1., 1.}, 1);     
	//		Matrix R = b.times(a); 
	//		Log.d("MatrixPosition",Arrays.deepToString(R.getArray()));
	//	}

	
	@SuppressLint("WrongCall")
	public static synchronized void initMatrix() {
		mSquares = new Square[GameParameters.ROWS_COLUMNS][GameParameters.ROWS_COLUMNS]; 
		for (int i = 0; i < GameParameters.ROWS_COLUMNS; i++) {
			for (int j = 0; j < GameParameters.ROWS_COLUMNS; j++) {
				MatrixPosition.mSquares[i][j] = new Square(i,j,gl);
			}
		}
	}

	private static synchronized void rotateMatrixRight(int row_cols,int num_left_right) {
		Square[][] result = new Square[row_cols][row_cols]; 
		for (int i = 0; i < row_cols; i++) {
			for (int j = 0; j < row_cols; j++) {
				result[i][j] = subSquares[row_cols-j-1][i];
				result[i][j].row = MatrixPosition.startRow + i;
				result[i][j].column = MatrixPosition.startColumn + j;
				result[i][j].rotation += 90;
				result[i][j].rotation = (result[i][j].rotation%360)==0 ? 0:result[i][j].rotation;
			}
		}
		subSquares = result;
	}

	private static synchronized void rotateMatrixLeft(int row_cols,int num_left_right) {
		Square[][] result = new Square[row_cols][row_cols]; 
		for (int i = 0; i < row_cols; i++) {
			for (int j = 0; j < row_cols; j++) {
				result[i][j] = subSquares[j][row_cols-i-1];
				result[i][j].row = MatrixPosition.startRow + i;
				result[i][j].column = MatrixPosition.startColumn + j;
				result[i][j].rotation -= 90;
				result[i][j].rotation = (result[i][j].rotation%360)==0 ? 0:result[i][j].rotation;
			}
		}
		subSquares = result;
	}

	/***
	 * Rotazione sottomatrici
	 * @param startX
	 * @param startY
	 * @param n
	 * @param num_left_right: numero di rotazione da effettuare a dx o a sx
	 * @throws IndexOutOfBoundsException
	 */
	public static int startRow = 0;
	public static int startColumn = 0;
	public static int squareDim = 0;
	public static int num_left_right = 0;
	public static int rotation = 0;
	public static int pivotX = 0;
	public static int pivotY = 0;
	
	public static synchronized void subMatrix(int startRow, int startColumn, int n, int num_left_right) throws IndexOutOfBoundsException{
		MatrixPosition.startRow = startRow;
		MatrixPosition.startColumn = startColumn;
		MatrixPosition.squareDim = n;
		MatrixPosition.num_left_right = num_left_right;
		if((startRow + n) > GameParameters.ROWS_COLUMNS || (startColumn + n) > GameParameters.ROWS_COLUMNS){
			throw new IndexOutOfBoundsException("fuori dai limiti della rotazione");
		}
		subSquares = new Square[n][n]; 
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				subSquares[i][j] = mSquares[startRow+i][startColumn+j];
			}
		}
		if (num_left_right > 0){	// ruota right #n
			for(int i=0;i<num_left_right;i++){
				rotateMatrixRight(n,num_left_right);
			}
		} 
		if (num_left_right < 0){	// ruota left #n
			num_left_right = num_left_right * -1;
			for(int i=0;i<num_left_right;i++){
				rotateMatrixLeft(n,num_left_right);
			}
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				mSquares[startRow+i][startColumn+j] = subSquares[i][j];
			}
		}
		dumpMatrix();
	}

	public static void markRotateAroundPosition(int startRow, int startColumn, int squareDim, int num_left_right, boolean isReady) {
		MatrixPosition.startRow = startRow;
		MatrixPosition.startColumn = startColumn;
		MatrixPosition.squareDim = squareDim;
		MatrixPosition.num_left_right = num_left_right;
		MatrixPosition.rotation = 0;
		
		float traslazioneOrigineAssiXY = GameParameters.ROWS_COLUMNS * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI)/2 + (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI)/2;
		float pX  = MatrixPosition.startColumn * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI) + (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI)/2 - traslazioneOrigineAssiXY;
		float pY  = MatrixPosition.startRow * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI) + (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI)/2 - traslazioneOrigineAssiXY;
		float p2X = pX + (MatrixPosition.squareDim) * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI);
		float p2Y = pY + (MatrixPosition.squareDim) * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI);
		
		MatrixPosition.pivotX = (int)(pX + p2X)/2;
		MatrixPosition.pivotY = (int)(pY + p2Y)/2;

		for (int i = 0; i < squareDim; i++) {
			for (int j = 0; j < squareDim; j++) {
				mSquares[startRow+i][startColumn+j].isReady = isReady;
			}
		}
//		dumpMatrix();
	}

	public static boolean checkSolution() {
		boolean solutionOk = true;
		for (int i = 0; i < GameParameters.ROWS_COLUMNS; i++) { // <--- ROWS
			for (int j = 0; j < GameParameters.ROWS_COLUMNS; j++) { //<--- COLS
				if(mSquares[i][j].rotation!=0 || mSquares[i][j].label_number != (GameParameters.ROWS_COLUMNS * (GameParameters.ROWS_COLUMNS - i - 1) + j +1)){
					solutionOk = false;
					Log.d(TAG,"-------->SOLUTION FALSE!");
					break;
				}
			}
		}
		return solutionOk;
	}

	public static void dumpMatrix(){
		StringBuffer line = new StringBuffer();
		for (int i = GameParameters.ROWS_COLUMNS-1;i>=0; i--) {
			for (int j = 0; j < GameParameters.ROWS_COLUMNS; j++) {
				line.append("["+mSquares[i][j].label_number+":"+mSquares[i][j].rotation+"]");
			}
			line.append("\n");
		}
		line.append("=========================\n");
		Log.d(TAG,line.toString());
	}
}