package com.hell.gun.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Environment;
import android.util.Log;

import com.hell.gun.GameParameters;
import com.hell.gun.Square;

public class AssetsLoader {
	private static final String TAG="AssetsLoader";
	private static AssetsLoader al = null; 
	public Context context = null;
	public Bitmap bk_bitmap = null;
	Paint paint = new Paint();

	private AssetsLoader(Context context){
		this.context = context;
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setColor(Color.RED);
		paint.setTextSize(40);
		paint.setFakeBoldText(true);
	}

	public static void init(Context context){
		AssetsLoader.al = new AssetsLoader(context);
	}

	public static AssetsLoader getInstance(){
		if(AssetsLoader.al == null){
			throw new RuntimeException("Must to initialize, call <init> first...");
		}
		return al;
	}

	public void initBackgroundBitmap(boolean force_reload_bk_default){
		Bitmap _bk_bitmap = null;
		if(force_reload_bk_default){
			_bk_bitmap = loadDefaultBitmap();
		}
		else{
			_bk_bitmap = _loadLastBitmap();
		}
		float target_w_h = (GameParameters.LATO_QUADRATO  + GameParameters.SPAZIO_TRA_QUADRATI) * GameParameters.ROWS_COLUMNS;
		float crop_w = _bk_bitmap.getWidth();// h =bitmap.getHeight();
		float scale = target_w_h / crop_w;
		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale);
		Log.d(TAG, "SCALE crop_w:"+crop_w);
		Log.d(TAG, "SCALE target_w_h:"+target_w_h);
		Log.d(TAG, "SCALE MATRIX:"+scale);
		this.bk_bitmap = Bitmap.createScaledBitmap(_bk_bitmap, (int)target_w_h, (int)target_w_h, false);
		Log.d(TAG, "SCALE bk_bitmap:"+this.bk_bitmap.getWidth());
		Log.d(TAG, "SCALE bk_bitmap:"+this.bk_bitmap.getHeight());

		//		this.bk_bitmap = Bitmap.createBitmap(bk_bitmap, 0, 0, (int)target_w_h, (int)target_w_h, matrix, false);

		// setting Square bitmap to NULL will FORCE texture!
		for (int i = 0; i < GameParameters.ROWS_COLUMNS; i++) {
			for (int j = 0; j < GameParameters.ROWS_COLUMNS; j++) {
				MatrixPosition.mSquares[i][j].bitmap = null;
			}
		}
	}

	public Bitmap loadDefaultBitmap(){
		Bitmap result = null;
		String name = "default.jpg";
		try {
			AssetManager assetManager = context.getAssets();
			InputStream istr = assetManager.open(name);
			result = BitmapFactory.decodeStream(istr);
			istr.close();
		} 
		catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	private Bitmap _loadLastBitmap(){
		Bitmap result = null;
		File tempDir= Environment.getExternalStorageDirectory();
		File path = new File (tempDir.getAbsolutePath()+"/rotosquare/output.jpg");
		Log.d(TAG,"PATH:"+path.toString());
		try {
			if(path.exists()){
				InputStream istr = new FileInputStream(path);
				result = BitmapFactory.decodeStream(istr);
				istr.close();
			}
			else{
				result = loadDefaultBitmap();
			}
		} 
		catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public void setBitmap(Square square){
		int startX =  (int)(square.startColumn * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI));
		//		int startX =  (int)(square.startColumn * GameParameters.LATO_QUADRATO + GameParameters.ROWS_COLUMNS * GameParameters.SPAZIO_TRA_QUADRATI);
		int startY =  (int)((GameParameters.ROWS_COLUMNS - 1 - square.startRow) * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI));
		//		int startY =  (int)((GameParameters.ROWS_COLUMNS - 1 - square.startRow) * GameParameters.LATO_QUADRATO + GameParameters.ROWS_COLUMNS * GameParameters.SPAZIO_TRA_QUADRATI);
		Log.d(TAG,"X:"+startX);
		Log.d(TAG,"Y:"+startY);
		square.bitmap = Bitmap.createBitmap(bk_bitmap, startX, startY, (int)GameParameters.LATO_QUADRATO, (int)GameParameters.LATO_QUADRATO);	

		Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
		Bitmap bkBitmap = Bitmap.createBitmap((int)GameParameters.LATO_QUADRATO, (int)GameParameters.LATO_QUADRATO, conf); // this creates a MUTABLE bitmap
		Canvas canvas = new Canvas(bkBitmap);

		Paint paintSh = new Paint();
		paintSh.setStyle(Paint.Style.FILL);
		//		paintSh.setStrokeWidth(GameParameters.RADIUS_ROUND);
		//		paintSh.setColor(Color.GREEN);
		BitmapShader shader = new BitmapShader(square.bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
		paintSh.setAntiAlias(false);
		paintSh.setShader(shader);
		RectF rect = new RectF(0,0,GameParameters.LATO_QUADRATO,GameParameters.LATO_QUADRATO);
		canvas.drawRoundRect(rect, GameParameters.RADIUS_ROUND, GameParameters.RADIUS_ROUND, paintSh);
		//		canvas.drawCircle(50, 50, 50, paintSh);

		square.bitmap = bkBitmap;

		/***

		BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
		Paint paintSh = new Paint();
		paintSh.setAntiAlias(true);
		paintSh.setShader(shader);
		canvas.drawBitmap(bitmap, GameParameters.LATO_QUADRATO, GameParameters.LATO_QUADRATO, paintSh);

		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(5);
		RectF rf = new RectF(0,GameParameters.LATO_QUADRATO,GameParameters.LATO_QUADRATO,0);
		canvas.drawRoundRect(rf,5f,5f,paint);
		paint.setColor(Color.WHITE);
		 ***/

		square.label_number = (GameParameters.ROWS_COLUMNS * (GameParameters.ROWS_COLUMNS - square.row - 1) + square.column +1);
		canvas.drawText(""+square.label_number, 20, 60, paint);

		//		paint.setColor(Color.BLACK);
		//		paint.setStyle(Paint.Style.STROKE);
		//		paint.setStrokeWidth(5);
		//		canvas.drawRect(tileArea,paint);
		//		paint.setColor(Color.WHITE);
		//		canvas.drawRoundRect(tileArea, 12, 12, paint);
	}
}
