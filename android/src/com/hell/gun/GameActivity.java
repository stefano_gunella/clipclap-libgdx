package com.hell.gun;

import java.io.File;

import android.app.ActionBar;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.TextView;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
//import com.hell.gun.android.R;
import com.hell.gun.glx.GlxGameFragment;
import com.hell.gun.util.AssetsLoader;
import com.hell.gun.util.MatrixPosition;
import com.badlogic.gdx.backends.android.AndroidFragmentApplication;

public class GameActivity extends FragmentActivity implements AndroidFragmentApplication.Callbacks{
	final String TAG = "GameActivity";
	Animation pulse_animation = null;
//	private GameView gameView;
	private GlxGameFragment gameView;
	public Chronometer chrono = null;
	public static int actionBarHeight = 0;
	public static boolean solved = false;
	public static boolean start_to_solve = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AssetsLoader.init(this);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setFormat(PixelFormat.TRANSPARENT);
		setContentView(R.layout.game);

		// Create libgdx fragment
		GlxGameFragment glxGameFragment = new GlxGameFragment();
        getSupportFragmentManager().beginTransaction().
                add(R.id.gameFragment, glxGameFragment).
                commit();
		
		pulse_animation = AnimationUtils.loadAnimation(this, R.anim.pulse_animation);
		initializeButtons();
		chrono = (Chronometer)findViewById(R.id.chronometer);
		/*
		findViewById(R.id.panel).post(new Runnable()
			{
				public void run(){
					_initHeightTitleBar();
				}
			}
		);

/*** rimozione pubblicità
		// Look up the AdView as a resource and load a request.
		AdView adView = (AdView) this.findViewById(R.id.adView);
	    
		AdRequest adRequest = new AdRequest.Builder()
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
		.addTestDevice("9587D3A8BB3BEE97BC5841EF50DC909F")
		.build();	
// WARNING:il TESTDEVICE lo si legge dal LOG.
// 12-31 18:46:14.356: I/Ads(29440): Use AdRequest.Builder.addTestDevice("9587D3A8BB3BEE97BC5841EF50DC909F") to get test ads on this device.
		*/

		//adView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		//adView.loadAd(adRequest);
 	}

 	/***
	private void _initHeightTitleBar(){
		ActionBar actionBar = getActionBar();
		actionBarHeight = actionBar.getHeight();
		Log.d(TAG, "--->actionBarHeight:"+actionBarHeight);
	}
 	***/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater=getMenuInflater();
		inflater.inflate(R.menu.options, menu);
		return true;
	}
	
	static boolean default_background = true;
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.option_menu_clear_bk_image:
			AssetsLoader.getInstance().initBackgroundBitmap(true);
//			gameView.requestRender();
			File tempDir= Environment.getExternalStorageDirectory();
			File path = new File (tempDir.getAbsolutePath()+"/clipclap/output.jpg");
			if(path.exists()){
				path.delete();
			}
			break;
		case R.id.option_menu_easy:
			GameParameters.ROWS_COLUMNS = 3;
			GameParameters.MIN_DIMENSIONE_QUADRATO = 2;
			GameParameters.MAX_DIMENSIONE_QUADRATO = 2;
			GameParameters.LATO_QUADRATO = 128;
			reset(null);
//			MatrixPosition.initMatrix();
//			AssetsLoader.getInstance().initBackgroundBitmap(false);
//			gameView.requestRender();
			break;
		case R.id.option_menu_normal:
			GameParameters.ROWS_COLUMNS = 4;
			GameParameters.MIN_DIMENSIONE_QUADRATO = 2;
			GameParameters.MAX_DIMENSIONE_QUADRATO = 2;
			GameParameters.LATO_QUADRATO = 128;
			reset(null);
//			MatrixPosition.initMatrix();
//			AssetsLoader.getInstance().initBackgroundBitmap(false);
//			gameView.requestRender();
			break;
		case R.id.option_menu_hard:
			GameParameters.ROWS_COLUMNS = 5;
			GameParameters.MIN_DIMENSIONE_QUADRATO = 2;
			GameParameters.MAX_DIMENSIONE_QUADRATO = 3;
			GameParameters.LATO_QUADRATO = 128;
			reset(null);
			break;
		}
		return true;
	}

	public void reset(View v) {
		synchronized (this) {
			Log.d(TAG, "reset");
			findViewById(R.id.button_reset).setEnabled(false);
			findViewById(R.id.button_shake).setEnabled(false);
			MatrixPosition.initMatrix();
			AssetsLoader.getInstance().initBackgroundBitmap(false);
			resetSelectedBox();
//			gameView.requestRender();
			findViewById(R.id.button_reset).setEnabled(true);
			findViewById(R.id.button_shake).setEnabled(true);
			resetChronometer();
			resetCounter();
			start_to_solve = false;
			solved = false;
			findViewById(R.id.button_shake).startAnimation(pulse_animation);
		}
	}

	private void resetSelectedBox() {
		Square.firstTouchedSquare = null;	
		Square.secondTouchedSquare = null;
	}

	public void shake(View v) {
		synchronized (this) {
			Log.d(TAG, "shake");
			if(solved){
				resetChronometer();
				resetCounter();
				solved = false;
			}
			findViewById(R.id.button_reset).setEnabled(false);
			findViewById(R.id.button_shake).setEnabled(false);
//			gameView.shake();
			start_to_solve = true;
			findViewById(R.id.button_shake).clearAnimation();
		}
	}

	public void initializeButtons(){	
		runOnUiThread(new Runnable(){
			public void run(){
				start_to_solve = false;
				findViewById(R.id.button_shake).startAnimation(pulse_animation);				
			}
		});
	}

	public static boolean startedChrono = false;//FIXME:controllare stato di attivazione chrono
	public void startChronometer() {
		if(startedChrono) 
			return;
		synchronized (this) {
			startedChrono = true;
			Log.d(TAG, "startChronometer");
			chrono.setBase(SystemClock.elapsedRealtime());
			chrono.start();
		}
	}

	public void resetChronometer() {
		startedChrono = false;
		chrono.setBase(SystemClock.elapsedRealtime());
		chrono.stop();
	}

	public void stopChronometer() {
		if(!startedChrono) 
			return;
		synchronized (this) {
			startedChrono = false;
			Log.d(TAG, "stopChronometer");
			chrono.stop();
		}
	}

	int moveCounter = 0;
	
	public void increaseCounter() {
		synchronized (this) {
			if(!GameView.isShaking){
				runOnUiThread(new Runnable(){
					public void run(){
						moveCounter++;
						Log.d(TAG, "increaseCounter");
						((TextView)findViewById(R.id.label_move_counter)).setText("MOVE#"+moveCounter);						
					}
				});
			}
		}
	}

	public void resetCounter() {
		moveCounter = 0;
		((TextView)findViewById(R.id.label_move_counter)).setText("MOVE#");
	}


	@Override
	protected void onPause() {
		super.onPause();
		//		mGLView.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		//		mGLView.onResume();
	}

	/***********************************
	 *  GET IMAGE FROM GALLERY
	 ***********************************/
	public void cropImageFromGallery(View v) {
		synchronized (this) {
			Log.d(TAG, "cropImageFromGallery");
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);

			intent.putExtra("crop", "true");
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("outputX", 400);
			intent.putExtra("outputY", 400);
			//			intent.putExtra("return-data", true);
			//			File tempFile = new File ("/sdcard/camera.jpg");
			File tempDir= Environment.getExternalStorageDirectory();
			File path = new File (tempDir.getAbsolutePath()+"/clipclap");
			path.mkdir();
			File tempFile= new File (path,"output.jpg");
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
			//			intent.putExtra ("output", Uri.fromFile(tempFile)) ;
			try {
				startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_GALLERY);
			}
			catch (ActivityNotFoundException e) {
				// Do nothing for now
			}
		}
	}

	static final int PICK_FROM_GALLERY = 1;

	public Uri selectedImageUri = null;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult");
		if (requestCode == PICK_FROM_GALLERY && null!=data) {
			selectedImageUri = data.getData();

			Handler handler = new Handler(); 
			handler.postDelayed(new Runnable() { 
				public void run() { 
					AssetsLoader.getInstance().initBackgroundBitmap(false);
//					gameView.requestRender();
				} 
			}, 500);
		}
	}

	@Override
	public void exit() {
		// TODO Auto-generated method stub
		
	}
}