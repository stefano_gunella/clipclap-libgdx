package com.hell.gun;

public class GameParameters {
	public static int NUM_SHAKE_MOVE = 25;						// numero di mosse per mescolare la scacchiera
	public static int ROWS_COLUMNS = 3;							// dimensione della scacchiera
	public static int MAX_DIMENSIONE_QUADRATO = 2; 				// MAX # di quadrati selezionabili per lato
	public static int MIN_DIMENSIONE_QUADRATO = 2; 				// MIN # di quadrati selezionabili per lato
	public static float LATO_QUADRATO = 128.0f;			// lato della casella quadrata 
	public static final int RADIUS_ROUND = 10;					// raggio per il bordo arrotondato delle caselle
	public static final float LATO_TEXTURE = 128.0f;
	public static final float SPAZIO_TRA_QUADRATI = 3.0f;		// spazio tra i quadrati
	public static final long TIME_TO_ROTATION_MILLISEC = 5L; 	// tempo di rotazione di una selezione
}
