package com.hell.gun;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.MotionEvent.PointerCoords;

import com.hell.gun.util.AssetsLoader;
import com.hell.gun.util.MatrixPosition;

public class Square {
	private static final String TAG = "Square";

	public int startRow = 0;	// riga iniziale matrice posizioni
	public int startColumn = 0;	// colonna iniziale matrice posizioni
	public int row = 0;			// riga appartenenza nella matrice delle posizioni
	public int column = 0;		// colonna appartenenza nella matrice delle posizioni
	public int label_number = 0;	// numero della tessera

	public float posX = 0;		// posizione corrente X
	public float posY = 0;		// posizione corrente Y
	public float rotation = 0;	// rotazione della cella:0/90/180/270
	public boolean isReady = false;	// marcatore per rotazione.
	public Bitmap bitmap = null;// immagine utilizzata per TEXTURE
	public int textureID = 0;	// id immagine utilizzata per TEXTURE
	public GL10 gl = null;

	public Square(int row, int column, GL10 gl){
		this.startRow = row;
		this.startColumn = column;
		this.row = row;
		this.column = column;
		this.gl = gl;
		_updatePosition();
	}

	private void _updatePosition() {
		posX = column * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI) - (GameParameters.ROWS_COLUMNS * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI) /2) + (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI) /2;
		posY = row * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI) - (GameParameters.ROWS_COLUMNS * (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI) /2) + (GameParameters.LATO_QUADRATO + GameParameters.SPAZIO_TRA_QUADRATI) /2;
	}

	public void draw(){
		_createObject();
		_selectedSquare();
		_addTexture();
		_updatePosition();
		_drawAtPosition(posX,posY);
	}

	public void drawAtTouch(){
		posX = GameRenderer.touchX;
		posY = GameRenderer.touchY;
		_createObject();
		_addTexture();
		_drawAtPosition(posX,posY);
	}
	
	public void rotateAroundPosition(){		
		_createObject();
		_updatePosition();
		_rotateAroundPosition(MatrixPosition.pivotX,MatrixPosition.pivotY,MatrixPosition.rotation);
	}

	public void rotateAroundPosition(float pX, float pY, float angle){
		_createObject();
		_updatePosition();
		_rotateAroundPosition(pX,pY,angle);
	}

	FloatBuffer vertices = null;
	int VERTEX_SIZE = 0;
	private void _createObject(){
		//		grandezza in bytes occupata da un vertice
		//		Ricordiamo che: 1 float = 4 bytes
		//		2 float per le coordinate x,y
		//		4 float per il colore
		//		==> (2 + 4) * 4 bytes
		//      int VERTEX_SIZE = (2 + 4) * 4; 
		VERTEX_SIZE = (2+2)*4; 
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4 * VERTEX_SIZE);// alloca lo spazio per un vertice
		byteBuffer.order(ByteOrder.nativeOrder());
		vertices = byteBuffer.asFloatBuffer(); // contenuto buffer in float
		vertices.put(
				new float[] { 
						-GameParameters.LATO_QUADRATO/2, -GameParameters.LATO_QUADRATO/2,	//coordinate I   vertice - 2 float
						0,1, 			//texture 2 float
						GameParameters.LATO_QUADRATO/2, -GameParameters.LATO_QUADRATO/2,	//coordinate II  vertice - 2 float
						1,1, 			//texture 2 float
						GameParameters.LATO_QUADRATO/2, GameParameters.LATO_QUADRATO/2,		//coordinate III vertice - 2 float
						1,0, 			//texture 2 float
						-GameParameters.LATO_QUADRATO/2, GameParameters.LATO_QUADRATO/2,	//coordinate IV vertice - 2 float
						0,0, 			//texture 2 float
				});
		vertices.flip();
	}

	/***
	 * Seleziona i quadrati selezionati per la rotazione.
	 */
	private void _selectedSquare() {
		if(firstTouchedSquare!=null && secondTouchedSquare!=null){
			int minRow = Math.min(firstTouchedSquare.row, secondTouchedSquare.row);
			int minColumn = Math.min(firstTouchedSquare.column, secondTouchedSquare.column);
			int maxRow = Math.max(firstTouchedSquare.row, secondTouchedSquare.row);
			int maxColumn = Math.max(firstTouchedSquare.column, secondTouchedSquare.column);
			if(	!GameView.isShaking 
				&& (this.row >= minRow && this.row <= maxRow) 
				&& (this.column >= minColumn && this.column <= maxColumn) 
				&& maxRow<(minRow+GameParameters.MAX_DIMENSIONE_QUADRATO) 
				&& maxColumn<(minColumn+GameParameters.MAX_DIMENSIONE_QUADRATO)){
				gl.glColor4f(0, 1, 0, 1);
			}
			else{
				gl.glColor4f(1, 1, 1, 1);
			}
		}
	}

	private void _drawAtPosition(float pX, float pY){
		short indices[] = {	
				0,1,2,
				3,0,2 
		};
		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(6*2);
		byteBuffer2.order(ByteOrder.nativeOrder());
		ShortBuffer elements = byteBuffer2.asShortBuffer();
		elements.put(indices);
		elements.flip();

		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glTranslatef(pX, pY, 0);
		if(rotation!=0){
			gl.glRotatef(rotation, 0, 0, 1);	// Ruoto il mio oggetto
		}

		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);

		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		vertices.position(0);
		gl.glVertexPointer(2, GL10.GL_FLOAT, VERTEX_SIZE, vertices);
		vertices.position(2);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, VERTEX_SIZE, vertices);
		gl.glDrawElements(GL10.GL_TRIANGLES, 6, GL10.GL_UNSIGNED_SHORT, byteBuffer2);
		//      Log.d(TAG,"_drawAtPositionX->"+posX+" drawAtPositionY->"+posY);
	}

	private void _addTexture() {
		if(null==this.bitmap){
//			Log.d(TAG,"_______addTexture______ROW:"+startRow+",COLUMN"+startColumn);
			AssetsLoader.getInstance().setBitmap(this);
			//		gl.glEnable(GL10.GL_TEXTURE_2D);
			int texturesIds[] = new int[1];
			gl.glGenTextures(1, texturesIds, 0);
			textureID = texturesIds[0];
			gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_NEAREST);
		}
	}

	/***
	 * Punto intorno a cui si vuole ruotare l'oggetto
	 * @param pX
	 * @param pY
	 * @param angle: 
	 * 	se positivo -> rotazione antioraria (aggiungo angolo)
	 * 	se negativo -> rotazione oraria (sottraggo angolo)
	 */
	private void _rotateAroundPosition(float pivotX, float pivotY, float angle){
		short indices[] = {	
				0,1,2,
				3,0,2 
			};
		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(6*2);
		byteBuffer2.order(ByteOrder.nativeOrder());
		ShortBuffer elements = byteBuffer2.asShortBuffer();
		elements.put(indices);
		elements.flip();

		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();
		
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);

		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		vertices.position(0);
		gl.glVertexPointer(2, GL10.GL_FLOAT, VERTEX_SIZE, vertices);
		vertices.position(2);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, VERTEX_SIZE, vertices);
		
		gl.glTranslatef(pivotX, pivotY, 0);		// riposiziono il centro al suo posto
		gl.glRotatef(angle, 0, 0, 1);			// Ruoto il mio oggetto 
		gl.glTranslatef(-pivotX, -pivotY, 0);	// Sposto il centro di rotazione nel punto attorno al quale voglio la rotazione.
		gl.glTranslatef(posX, posY, -0.1f);		// Sistemo l'oggetto nella sua posizione.
		gl.glRotatef(rotation, 0, 0, 1);
		
		gl.glDrawElements(GL10.GL_TRIANGLES, 6, GL10.GL_UNSIGNED_SHORT, byteBuffer2);
		gl.glMatrixMode(GL10.GL_PROJECTION);

		
//		gl.glTranslatef(pivotX, pivotY, 0);		// riposiziono il centro al suo posto
//		gl.glRotatef(angle, 0, 0, 1);			// Ruoto il mio oggetto 
//		_drawAtPosition(posX, posY);
	}

	public static Square firstTouchedSquare = null;
	public void setFirstTouched(PointerCoords pc){
		double dist_2_center = Math.sqrt(Math.pow((pc.x - posX),2)+Math.pow((pc.y - posY),2));
		if(dist_2_center < GameParameters.LATO_QUADRATO/2){
			firstTouchedSquare = this;
			secondTouchedSquare = this;
			Log.d(TAG,"FIRST row->"+row+"\n FIRST column->"+column + "\n D:" +dist_2_center);
		}
	}

	public static Square secondTouchedSquare = null;
	public void setSecondTouched(PointerCoords pc){
		double dist_2_center = Math.sqrt(Math.pow((pc.x - posX),2)+Math.pow((pc.y - posY),2));
		boolean chek_diagonal = Math.abs(firstTouchedSquare.row - row) == Math.abs(firstTouchedSquare.column - column);
		if((dist_2_center < GameParameters.LATO_QUADRATO/2) && chek_diagonal){
			secondTouchedSquare = this;
			Log.d(TAG,"diff row->"+Math.abs(firstTouchedSquare.row - secondTouchedSquare.row)+"\n");
			Log.d(TAG,"diff column->"+Math.abs(firstTouchedSquare.column - secondTouchedSquare.column)+"\n");
			Log.d(TAG,"--->"+chek_diagonal);
		}
	}
}
