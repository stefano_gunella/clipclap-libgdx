package com.hell.gun;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent.PointerCoords;

import com.hell.gun.util.AssetsLoader;
import com.hell.gun.util.FPSCounter;
import com.hell.gun.util.MatrixPosition;

public class GameRenderer implements GLSurfaceView.Renderer {
	private static final String TAG = "GameRenderer";
	public static float touchX = 0;
	public static float touchY = 0;
	public GameView parentView = null;
	private int width;
	private int height;

	public GameRenderer(GameView parentView, int width, int height) {
		this.width = width;
		this.height = height;
		this.parentView = parentView;
		Log.d(TAG,"GameRenderer");
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		MatrixPosition.gl=gl;
		MatrixPosition.initMatrix();
		AssetsLoader.getInstance().initBackgroundBitmap(false);
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrthof(-width/2, width/2, -height/2, height/2, 1, -1); // view frustum

		//		gl.glDisable(GL10.GL_DITHER);
		//		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);
		//		gl.glClearColor(0,0,0,0);
		//		gl.glClearColor(0,0,0,1);
		//		gl.glEnable(GL10.GL_CULL_FACE);
		//		gl.glShadeModel(GL10.GL_SMOOTH);
		//		gl.glEnable(GL10.GL_DEPTH_TEST);
		//		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
	} 

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		// TODO Auto-generated method stub
	}

	FPSCounter fpsCounter = new FPSCounter();
	@Override
	public void onDrawFrame(GL10 gl) {
//		gl.glEnable(GL10.GL_DEPTH_TEST);
//		gl.glDepthFunc(GL10.GL_NEVER);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glEnable(GL10.GL_DEPTH_TEST);
		for (int i = 0; i < GameParameters.ROWS_COLUMNS; i++) {
			for (int j = 0; j < GameParameters.ROWS_COLUMNS; j++) {
				if(MatrixPosition.mSquares[i][j].isReady){
					MatrixPosition.mSquares[i][j].rotateAroundPosition();
				}
				else{
					MatrixPosition.mSquares[i][j].draw();
				}
//            	MatrixPosition.mSquares[i][j].drawAtTouch();
//            	MatrixPosition.mSquares[i][j].rotateAroundPosition(165,165,rad);
//            	MatrixPosition.mSquares[i][j].rotateAroundPosition(pivotX,pivotY,rad);
			}
		}
		gl.glDisable(GL10.GL_DEPTH_TEST);
		fpsCounter.logFrame();
	}

	public void setFirstSquare(PointerCoords pc) {
		for (int i = 0; i < GameParameters.ROWS_COLUMNS; i++) {
			for (int j = 0; j < GameParameters.ROWS_COLUMNS; j++) {
				MatrixPosition.mSquares[i][j].setFirstTouched(pc);
//				Log.d(TAG,"========== FirstSquare :"+Square.firstTouchedSquare);
			}
		}
	}

	public void setSecondSquare(PointerCoords pc) {
		for (int i = 0; i < GameParameters.ROWS_COLUMNS; i++) {
			for (int j = 0; j < GameParameters.ROWS_COLUMNS; j++) {
				MatrixPosition.mSquares[i][j].setSecondTouched(pc);	
			}
		}
	}

	public void resetSelectedSquare() {
		Square.firstTouchedSquare = null;
		Square.secondTouchedSquare = null;	
	}

	class RotationThread extends Thread{
		int min_row, min_column, lato;
		
		public RotationThread(int min_row, int min_column, int lato){
			this.min_row = min_row;
			this.min_column = min_column;
			this.lato = lato;
		}
		
		@Override
		public void run(){
			if(lato>=GameParameters.MIN_DIMENSIONE_QUADRATO && lato<=GameParameters.MAX_DIMENSIONE_QUADRATO){ 	// quadrato con lato minimo di DIMENSIONE_QUADRATO caselle
				// num_left_right>0 numero di giri in senso orario
				// num_left_right<0 numero di giri in senso antiorario
				// alla fine sar� sempre +-1
				int num_left_right =  0;
				int sign_row = (int)Math.signum(Square.secondTouchedSquare.row - Square.firstTouchedSquare.row);
				int sign_column = (int)Math.signum(Square.secondTouchedSquare.column - Square.firstTouchedSquare.column);
				if(sign_row>0 && sign_column>0)
					num_left_right = -1;
				else if(sign_row<0 && sign_column<0)
					num_left_right = 1;
				else if(sign_row<0 && sign_column>0)
					num_left_right = -1;
				else if(sign_row>0 && sign_column<0)
					num_left_right = 1;
				
				MatrixPosition.markRotateAroundPosition(min_row,min_column,lato,num_left_right,true);
				for (int i=0;i<=90;i=i+2){
					MatrixPosition.rotation = i * num_left_right;
					parentView.requestRender();
					try {
						RotationThread.sleep(GameParameters.TIME_TO_ROTATION_MILLISEC);
					}
					catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				MatrixPosition.markRotateAroundPosition(min_row,min_column,lato,0,false);
				MatrixPosition.subMatrix(min_row, min_column, lato, num_left_right);
				parentView.activity.increaseCounter();
				parentView.requestRender();
			}
		}
	}

	public void rotateSelectedSquare() {
		int min_row = Math.min(Square.firstTouchedSquare.row, Square.secondTouchedSquare.row);
		int min_column = Math.min(Square.firstTouchedSquare.column, Square.secondTouchedSquare.column);
		int lato = 1 + Math.abs(Square.firstTouchedSquare.row - Square.secondTouchedSquare.row);
		RotationThread thRotation = new RotationThread(min_row,min_column,lato);
		thRotation.start();
		try {
			thRotation.join();
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		resetSelectedSquare();
	}
}
