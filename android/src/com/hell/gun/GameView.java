package com.hell.gun;

import java.util.Random;

//import com.hell.gun.android.R;
import com.hell.gun.util.MatrixPosition;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.opengl.GLSurfaceView;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.MotionEvent.PointerCoords;

public class GameView extends GLSurfaceView {
	public static final String TAG = "GameView";
	public int width=0;
	public int height=0;
	public GameRenderer gameRenderer = null;
	SoundPool sp = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
	GameActivity activity = null;

	public GameView(Context context, AttributeSet attrs){
		super(context,attrs);
		DisplayMetrics metrics = new DisplayMetrics();
		activity = (GameActivity)context;
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		width = metrics.widthPixels;
		height = metrics.heightPixels-GameActivity.actionBarHeight;
		this.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
		gameRenderer = new GameRenderer(this,width,height);
		this.setRenderer(gameRenderer);
		this.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
//		this.getHolder().setFormat(PixelFormat.RGBA_8888);
//		this.getHolder().setFormat(PixelFormat.TRANSLUCENT);
		this.setKeepScreenOn(true);
//		this.setDrawingCacheEnabled(true);
//		this.setZOrderOnTop(true);
//		MatrixPosition.dumpMatrix();
		soundIds[0] = sp.load(getContext(), R.raw.slide_tile, 1);
		soundIds[1] = sp.load(getContext(), R.raw.applause, 1);
	}

	public boolean onTouchEvent(MotionEvent motionEvent) {
		Log.d(TAG, "-----------onTouchEvent--------------");
		if (isShaking || !GameActivity.start_to_solve){
			return false;
		}
//		int pointerIndex = ((motionEvent.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT);
		int pointCnt = motionEvent.getPointerCount();
//		Log.d(TAG,"POINTER COUNT--->"+pointCnt);
		Log.d(TAG,"actionBarHeight--->"+GameActivity.actionBarHeight);
		GameRenderer.touchX = motionEvent.getX()-width/2;
		GameRenderer.touchY = -GameActivity.actionBarHeight-motionEvent.getY()+height/2;
		PointerCoords pc = new PointerCoords();
//		for (int i = 0; i < pointCnt; i++) {
//			motionEvent.getPointerCoords(i, pc);
//			Log.d(TAG,i+ " POINTER COORD X -->"+pc.x);
//			Log.d(TAG,i+ " POINTER COORD Y -->"+pc.y);
//		}
		switch (motionEvent.getAction()) {
			case MotionEvent.ACTION_DOWN:
				activity.startChronometer();
				motionEvent.getPointerCoords(0, pc);
				pc.x = motionEvent.getX()-width/2;
				pc.y = -GameActivity.actionBarHeight-motionEvent.getY()+height/2;
				gameRenderer.setFirstSquare(pc);	// primo quadrato a essere selezionato
				requestRender();
				break;
			case MotionEvent.ACTION_MOVE:
				motionEvent.getPointerCoords(0, pc);
				pc.x = motionEvent.getX()-width/2;
				pc.y = -GameActivity.actionBarHeight-motionEvent.getY()+height/2;
				gameRenderer.setSecondSquare(pc);	// secondo quadrato a essere selezionato
				requestRender();
				break;
			case MotionEvent.ACTION_UP:
				gameRenderer.rotateSelectedSquare();
				if(null != Square.firstTouchedSquare && Square.firstTouchedSquare != Square.secondTouchedSquare){
					playSoudEffect(0);
				}
				else{
					Square.firstTouchedSquare = null;
					Square.secondTouchedSquare = null;
				}
				requestRender();
				if (MatrixPosition.checkSolution()){
					activity.stopChronometer();
					GameActivity.solved = true;
					playSoudEffect(1);
					long[] pattern = {0, 500, 1000, 500, 1000, 500};
					Vibrator v = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
					v.vibrate(pattern,-1);
					activity.initializeButtons();
				}
				break;
		}
		return true;
	}
	
	/**************************************/
	public void shake() {
		ShakeTileThread st = new ShakeTileThread();
		st.start();
	}

	public static boolean isShaking = false;
	class ShakeTileThread extends Thread {
		int last_row=0;
		int last_column=0;
		@Override
		public void run() {
			isShaking = true;
			Random rnd = new Random();
			for (int i = 0; i < GameParameters.NUM_SHAKE_MOVE; i++) {
				int r = 0;
				int c = 0;
				while(r==last_row && c==last_column){
					r = rnd.nextInt(GameParameters.ROWS_COLUMNS-GameParameters.MAX_DIMENSIONE_QUADRATO+1);
					c = rnd.nextInt(GameParameters.ROWS_COLUMNS-GameParameters.MAX_DIMENSIONE_QUADRATO+1);
				}
				last_row = r;
				last_column = c;
				Square.firstTouchedSquare = MatrixPosition.mSquares[r][c];
				Square.secondTouchedSquare = MatrixPosition.mSquares[r+GameParameters.MAX_DIMENSIONE_QUADRATO-1][c+GameParameters.MAX_DIMENSIONE_QUADRATO-1];
//				Log.d(TAG, "-------------------------------------------");
//				Log.d(TAG, "ShakeTileThread r1:"+Square.firstTouchedSquare.row +" c1:"+Square.firstTouchedSquare.column);
//				Log.d(TAG, "ShakeTileThread r2:"+Square.secondTouchedSquare.row +" c2:"+Square.secondTouchedSquare.column);
				if(rnd.nextInt(2)==0){
					Square tmp = Square.firstTouchedSquare;
					Square.firstTouchedSquare  = Square.secondTouchedSquare;
					Square.secondTouchedSquare = tmp;
				}
				gameRenderer.rotateSelectedSquare();
			}

			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					activity.findViewById(R.id.button_reset).setEnabled(true);
					activity.findViewById(R.id.button_shake).setEnabled(true);
				}
			});
			isShaking = false;
			Square.firstTouchedSquare = null;
			Square.secondTouchedSquare = null;
		}
	}

	int soundIds[] = new int[2];
	protected void playSoudEffect(int index){
		sp.play(soundIds[index], 1, 1, 0, 0, 1.0f);	
	}
}